column(width = 12, align = "center",
       selectInput(inputId = "general.cities", label = "Mesta:",
                   multiple = TRUE, choices = c()),
       sliderInput(inputId = "general.years", label = "Godine:",
                   min = 2000, max = 2030, value = c(2010, 2020), step = 1),
       actionButton(inputId = "general.submit", label = "Prikazi"),
       actionButton(inputId = "general.refresh", label = "Ponovo ucitaj podatke"),
       br(),
       br(),
       leafletOutput(outputId = "map.interactive"),
       br(),
       br(),
       plotOutput(outputId = "general.area"),
       br(),
       br(),
       plotOutput(outputId = "general.ave.area"),
       br(),
       br(),
       plotOutput(outputId = "general.height"),
       br(),
       br(),
       plotOutput(outputId = "general.blossoms"))
