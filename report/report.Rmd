---
output:
  pdf_document:
    keep_tex: true
    extra_dependencies:
      float: null
      booktabs: null
      longtable: null
      xcolor: ["table"]
header-includes:
- \definecolor{pmfgreen}{HTML}{E6EED5}
- \definecolor{osmblue}{HTML}{0078A8}
- \arrayrulecolor{pmfgreen}
urlcolor: osmblue
params:
  town: "Arezzo"
  year: 2000
  wd: "~/Ambrosia"
---

```{r echo = FALSE, fig.align = "center", message=FALSE, fig.width=7.27}
source("map.R", local = TRUE)$value
```

```{r echo = FALSE, fig.align = "center", message=FALSE}
source("table.R", local = TRUE)$value
```

<!--- Local Variables: -->
<!--- mode: markdown -->
<!--- End: -->
