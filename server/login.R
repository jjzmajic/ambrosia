rv$user <- data.frame(uname = input$uname,
                      passwd = as.vector(sha512(input$passwd)),
                      stringsAsFactors = FALSE)

if (rv$user$uname %in% rv$users$uname &&
    rv$user$passwd == rv$users$passwd[rv$user$uname == rv$users$uname]) {
  output$tabs <- renderUI({
    source(file = "./ui/non_admin_ui.R", local = TRUE)$value
  })

  if (rv$users$is.admin[rv$users$uname == rv$user$uname] == "TRUE") {
    output$tabs <- renderUI({
      source(file = "./ui/admin_ui.R", local = TRUE)$value
    })

    updateSelectInput(session = session,
                      inputId = "userdel.list",
                      choices = rv$users$uname)

    source(file = "./server/download.R", local = TRUE)$value
    source(file = "./server/download_consolidated.R", local = TRUE)$value
  }
} else {
  updateTextInput(session = session,
                  inputId = "passwd",
                  value = "")
  showNotification("Pogresno korisnicko ime ili lozinka.")
}
