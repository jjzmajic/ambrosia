if (!file.exists("consolidated_data.csv")) {
  cdata <- data.frame(geografska.sirina.biljke = character(),
                      geografska.duzina.biljke = character(),
                      geografska.sirina.okruzenja = character(),
                      geografska.duzina.okruzenja = character(),
                      vreme.fotografisanja.biljke = character(),
                      vreme.fotografisanja.okruzenja = character(),
                      visina.biljke = numeric(),
                      pokrivena.povrsina = numeric(),
                      prisustvo.cvasti = logical(),
                      korisnik = character(),
                      komentar = character(),
                      sifra = character(),
                      broj.ili.opis = numeric(),
                      adresa = character(),
                      godina = numeric(),
                      mesto = character(),
                      stringsAsFactors = FALSE
                      )
} else {

  cdata <- read.csv("consolidated_data.csv", stringsAsFactors=FALSE)
  cdata$vreme.fotografisanja.biljke <- as.Date(cdata$vreme.fotografisanja.biljke,
                                               tryFormats = c("%Y:%m:%d %H:%M:%S",
                                                             "%Y-%m-%d %H:%M:%S",
                                                             "%Y/%m/%d %H:%M:%S"))
}
